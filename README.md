## What is this?

This is a headless app that collects all our `fly.io` logs and ships them to our hosted Loki instance on Grafana Cloud. This is particularly useful if you output structured logs because it means that you can use Loki to filter and analyze said logs. It is on [code  provided by `fly.io`](https://github.com/superfly/fly-log-shipper)

It uses the most excellent [Vector](https://vector.dev/) to collect and ship logs.

You don't have to do anything special in your `fly.io` app to use this. You just have to log as normal.

So, for example, you send a log message that looks like this on the console:

```
{"message": "a normal log message", "method": "dork", "app": "my-webapp"}
```

You will be able to use Loki and Grafana to filter and derive stats on all the key/values in the message.

## Howto run 

To run this on `fly.io`, you need to set the following secrets:

- ACCESS_TOKEN (`fly secrets set ACCESS_TOKEN=$(fly auth token)`)
- LOKI_PASSWORD (lookup in our password manager)
- LOKI_URL (lookup in our password manager)
- LOKI_USERNAME (lookup in our password manager)
- ORG (This should be set to "crossref")


